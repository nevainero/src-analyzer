"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContentItem = void 0;
var path = require("path");
var fs = require("fs");
var ContentItem = /** @class */ (function () {
    function ContentItem(dir, item) {
        var contentPath = path.join(dir, item);
        var stat = fs.statSync(contentPath);
        if (stat.isFile() || stat.isDirectory())
            return {
                name: item,
                path: contentPath,
                type: stat.isFile() ? 'file' : 'dir',
                children: stat.isDirectory() ? new ContentItem(contentPath, item) : null
            };
    }
    return ContentItem;
}());
exports.ContentItem = ContentItem;
