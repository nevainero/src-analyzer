"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Tree = void 0;
var file_1 = require("./file");
var path = require('path');
var fs = require('fs');
var Tree = /** @class */ (function () {
    function Tree(config) {
        var _this = this;
        this.initProps = {
            lines: 0,
            todo: 0,
            classes: 0,
            comments: 0,
            interfaces: 0,
            emptyLines: 0,
            mobxComponents: 0
        };
        this.config = null;
        this.state = {
            tree: [],
            files: 0,
            props: this.initProps
        };
        this.fill = function () { return _this.state.tree = _this.getTree(_this.config.path); };
        this.getFileProps = function (isFile, contentPath) {
            var _a = _this.config, extensions = _a.extensions, exclude = _a.exclude;
            var name = path.basename(contentPath);
            var extension = path.extname(name);
            var extensionCheck = extensions.includes(extension.substr(1));
            var excludeCheck = !exclude.includes(name);
            if (isFile && extensionCheck && excludeCheck) {
                var data = fs.readFileSync(contentPath).toString();
                return new file_1.File(data).getProps();
            }
            else
                return null;
        };
        this.getTree = function (dir) {
            return fs.readdirSync(dir).map(function (item) {
                var contentPath = path.join(dir, item);
                var stat = fs.statSync(contentPath);
                var file = _this.getFileProps(stat.isFile(), contentPath);
                if (file !== null)
                    _this.incProps(file);
                var result = {
                    name: item,
                    path: contentPath,
                    type: stat.isFile() ? 'file' : 'dir',
                    file: file,
                    children: stat.isDirectory() ? _this.getTree(contentPath) : null
                };
                return result;
            });
        };
        this.incProps = function (props) {
            _this.state.files += 1;
            _this.state.props.todo += props.todo;
            _this.state.props.lines += props.lines;
            _this.state.props.classes += props.classes;
            _this.state.props.comments += props.comments;
            _this.state.props.emptyLines += props.emptyLines;
            _this.state.props.interfaces += props.interfaces;
            _this.state.props.mobxComponents += props.mobxComponents;
        };
        this.getProps = function () {
            console.log(_this.state.props);
            console.log('Files quantity: ' + _this.state.files);
        };
        this.config = config;
    }
    return Tree;
}());
exports.Tree = Tree;
