"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tree_1 = require("./tree");
var config = require("./config");
var tree = new tree_1.Tree(config);
tree.fill();
tree.getProps();
