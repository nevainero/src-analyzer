"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.File = void 0;
var File = /** @class */ (function () {
    function File(data) {
        var _this = this;
        this.data = [];
        this.find = function (str) {
            return _this.data.filter(function (line) { return line.includes(str); }).length;
        };
        this.getLines = function () { return _this.data.length; };
        this.getEmptyLines = function () { return _this.data.filter(function (line) { return line === ''; }).length; };
        this.getTodos = function () { return _this.find('// TODO'); };
        this.getClasses = function () { return _this.find('class'); };
        this.getComments = function () {
            return _this.find('//') + Math.min(_this.find('/*'), _this.find('*/'));
        };
        this.getProps = function () {
            return {
                lines: _this.data.length,
                emptyLines: _this.getEmptyLines(),
                todo: _this.find('// TODO'),
                classes: _this.find('class'),
                interfaces: _this.find('interface'),
                mobxComponents: _this.find('observer('),
                comments: _this.getComments()
            };
        };
        this.data = data.split('\n');
    }
    return File;
}());
exports.File = File;
