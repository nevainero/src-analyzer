import { Tree }    from './tree'
import * as config from './config'

const tree = new Tree(config)
tree.fill()

tree.getProps()
