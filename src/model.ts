export interface IState {
  tree: IContentItem[]
  files: number
  props: IProps
}

export interface IConfig {
  extensions: string[]
  exclude: string[]
  path: string
}

export interface IProps {
  todo: number
  lines: number
  classes: number
  comments: number
  emptyLines: number
  interfaces: number
  mobxComponents: number
}

export interface IContentItem {
  name: string
  path: string
  type: 'file' | 'dir'
  file: IProps | null
  children: IContentItem[] | null
}