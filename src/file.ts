import { IProps } from './model'

export class File {
  constructor (data: string) {
    this.data = data.split('\n')
  }

  data: string[] = []

  find = (str: string): number => {
    return this.data.filter((line) => line.includes(str)).length
  }

  getLines = () => this.data.length

  getEmptyLines = (): number => this.data.filter((line) => line === '').length

  getTodos = (): number => this.find('// TODO')

  getClasses = (): number => this.find('class')

  getComments = (): number => {
    return this.find('//') + Math.min(
      this.find('/*'), this.find('*/')
    )
  }

  getProps = (): IProps => {
    return {
      lines: this.data.length,
      emptyLines: this.getEmptyLines(),
      todo: this.find('// TODO'),
      classes: this.find('class'),
      interfaces: this.find('interface'),
      mobxComponents: this.find('observer('),
      comments: this.getComments()
    }
  }
}