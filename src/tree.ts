import { IConfig, IContentItem, IProps, IState } from './model'
import { File }                                  from './file'

const path = require('path')
const fs = require('fs')

export class Tree {
  constructor (config) {
    this.config = config
  }

  initProps: IProps = {
    lines: 0,
    todo: 0,
    classes: 0,
    comments: 0,
    interfaces: 0,
    emptyLines: 0,
    mobxComponents: 0
  }

  config: IConfig | null = null

  state: IState = {
    tree: [],
    files: 0,
    props: this.initProps
  }

  fill = () => this.state.tree = this.getTree(this.config.path)

  getFileProps = (isFile: boolean, contentPath: string): IProps | null => {
    const { extensions, exclude } = this.config
    const name = path.basename(contentPath)
    const extension = path.extname(name)
    const extensionCheck = extensions.includes(extension.substr(1))
    const excludeCheck = !exclude.includes(name)

    if (isFile && extensionCheck && excludeCheck) {
      const data = fs.readFileSync(contentPath).toString()
      return new File(data).getProps()
    } else return null
  }

  getTree = (dir: string): IContentItem[] => {
    return fs.readdirSync(dir).map((item) => {
      const contentPath = path.join(dir, item)
      const stat = fs.statSync(contentPath)
      const file = this.getFileProps(stat.isFile(), contentPath)
      if (file !== null) this.incProps(file)

      const result: IContentItem = {
        name: item,
        path: contentPath,
        type: stat.isFile() ? 'file' : 'dir',
        file: file,
        children: stat.isDirectory() ? this.getTree(contentPath) : null
      }
      return result
    })
  }

  incProps = (props: IProps) => {
    this.state.files += 1
    this.state.props.todo += props.todo
    this.state.props.lines += props.lines
    this.state.props.classes += props.classes
    this.state.props.comments += props.comments
    this.state.props.emptyLines += props.emptyLines
    this.state.props.interfaces += props.interfaces
    this.state.props.mobxComponents += props.mobxComponents
  }

  getProps = () => {
    console.log(this.state.props)
    console.log('Files quantity: ' + this.state.files)
  }
}